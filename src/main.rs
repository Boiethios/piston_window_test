extern crate piston_window;
extern crate sdl2_window;
extern crate opengl_graphics;

//use piston_window::*;
use piston_window::{RenderArgs, UpdateArgs, Key, PistonWindow, WindowSettings,
					Input, Button,
					rectangle, clear};
//use piston_window::math::{Matrix2d};
use opengl_graphics::{OpenGL, GlGraphics};

// Change this to OpenGL::V2_1 if not working.
const OPENGL: OpenGL = OpenGL::V3_2;

#[derive(Default)]
struct KeyState {
	pub left: bool,
	pub right: bool,
	pub up: bool,
	pub down: bool,
}


pub struct MyApp {
	square_radius: f64,
	square_rotation: f64,
	key_state: KeyState,
	gl: GlGraphics,	// OpenGL drawing backend.
}

impl Default for MyApp {
	fn default() -> Self {
		MyApp {
			square_radius: 0.1,
			square_rotation: 0.0,
			key_state: KeyState::default(),
			gl: GlGraphics::new(OPENGL),
		}
	}
}

impl MyApp {
	fn new() -> Self {
		Default::default()
	}

	fn render(&mut self, args: &RenderArgs) {
		use piston_window::Transformed;

		let green = [0.3, 0.9, 0.3, 1.0];
		let red   = [0.9, 0.3, 0.3, 1.0];

		let square = rectangle::centered_square(0.0, 0.0, self.square_radius);
		let rotation = self.square_rotation;
		let (w, h) = (args.width as f64, args.height as f64);

		self.gl.draw(args.viewport(), |c, gl| {
			let view = c.transform.scale(w, h);
			clear(green, gl);
			let transform = view.trans(0.5, 0.5).rot_rad(rotation);
			rectangle(red, square, transform, gl);
		});
	}

	fn update(&mut self, args: &UpdateArgs) {
		if self.key_state.left {
			self.square_rotation -= 2.0 * args.dt;
		}
		if self.key_state.right {
			self.square_rotation += 2.0 * args.dt;
		}
		if self.key_state.up {
			self.square_radius /= args.dt * 110.;
		}
		if self.key_state.down {
			self.square_radius *= args.dt * 110.;
		}
	}

	fn key_handler(&mut self, key: Key, state: bool	) {
		match key {
			Key::Left	=> self.key_state.left = state,
			Key::Right	=> self.key_state.right = state,
			Key::Up		=> self.key_state.up = state,
			Key::Down	=> self.key_state.down = state,
			_			=> (),
		}
	}
}

fn main() {
	type Window = PistonWindow<sdl2_window::Sdl2Window>;

	let mut window: Window = WindowSettings::new("spinning-square", [800, 800])
		.opengl(OPENGL).exit_on_esc(true).build().unwrap();
	let mut app = MyApp::new();

	while let Some(event) = window.next() {
		match event {
			Input::Render(args)					=> app.render(&args),
			Input::Press(Button::Keyboard(k))	=> app.key_handler(k, true),
			Input::Release(Button::Keyboard(k))	=> app.key_handler(k, false),
			Input::Update(args)					=> app.update(&args),
			_ => (),
		}
	};
}
